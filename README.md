# Summary
With delinearization, audience fragmentation, and the new habits of Millennials (in the broadest sense, which also includes the youngest generation, Generation Z), the visibility of public service media requires both content innovation and new online social interactions. It is a question of capturing attention by making the source of online content recognizable (akin to an inbound marketing strategy). Ultimately, the main objective of this project is to reach younger audiences by implementing a conversational tool, such as a chatbot.

To this end, the [Medi@LAB-Genève](https://www.unige.ch/sciences-societe/medialab/) institute of the [University of Geneva](https://www.unige.ch/en/) will first conduct a twofold survey (semi-directive interviews and an in-depth survey on practices), in order to define the functionalities of a chatbot tailored to the specific needs of the media partner, [RTS](https://www.rts.ch/), as well as to the target audiences. They will then conduct a state-of-the-art analysis of the innovations being developed around chatbots and artificial intelligence, drawing notably on their extensive international networks. Lastly, they will define, in synergy with RTS, the optimal methodology for targeting young audiences who are essentially connected to social platforms. The result should be a conversational tool that will be developed by students from the [Computer Science Center (CUI)](https://cui.unige.ch/en/) of the University of Geneva, under the supervision of the project coordinators and the guidance of RTS. By the end of the project, this chatbot will be deployed, tested and evaluated.

# Presentation and Demo
[![Presentation](https://img.youtube.com/vi/FkhcpU1E7r8/0.jpg)](https://www.youtube.com/watch?v=FkhcpU1E7r8 "Presentation")
[![Demo](https://img.youtube.com/vi/utZpF_ebDXE/0.jpg)](https://www.youtube.com/watch?v=utZpF_ebDXE "Demo")

# Outputs
- [AMEZ-DROZ, Philippe René. Rapport de recherche MILBOT: Livrable 1. 2021](https://archive-ouverte.unige.ch/unige:160391)
- [AMEZ-DROZ, Philippe René, PUGLISI, Laura, BADILLO, Patrick-Yves. Rapport de recherche MILBOT: Livrable 2, jalons 1 et 2. 2021](https://archive-ouverte.unige.ch/unige:160392)
- [AMEZ-DROZ, Philippe René, DI MARZO SERUGENDO, Giovanna, PUGLISI, Laura. Rapport de recherche MILBOT: Livrable 3. 2022](https://archive-ouverte.unige.ch/unige:160393)

# In the media
- [**Milbot, le chatbot qui ramène les millenials vers la télévision publique** (La revue européenne des médias et du numérique](https://la-rem.eu/2022/07/milbot-le-chatbot-qui-ramene-les-millenials-vers-la-television-publique/)
- [**Milbot, le chatbot qui veut ramener les mobinautes devant la télé suisse** (SmartBot)](https://www.smartbot.fr/2022/07/06/milbot-le-chatbot-qui-veut-ramener-les-mobinautes-devant-la-tele-suisse/)